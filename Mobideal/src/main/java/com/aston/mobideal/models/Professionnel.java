package com.aston.mobideal.models;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Professionnel {
	
	@Id
	private String id;
	private String nom;
	private String mail;
	private String telephone;
	private String adresse;
	private List<String> lienSociaux;
	private String password;
	private String secteurActivite;	
	private LocalDate dateInscription;
	

}
