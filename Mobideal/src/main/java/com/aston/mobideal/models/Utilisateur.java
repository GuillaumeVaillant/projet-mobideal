package com.aston.mobideal.models;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Utilisateur {
	
	@Id
	private String id;
	private String nom;
	private String prenom;
	private String adresse;
	private String telephone;
	private String mail;
	private String password;
	private List<Professionnel> Professionnels; 
	private LocalDate dateInscription;

}
