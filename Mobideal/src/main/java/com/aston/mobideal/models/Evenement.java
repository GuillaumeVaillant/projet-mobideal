package com.aston.mobideal.models;

import java.time.LocalDate;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
public class Evenement {
	
	@Id
	private String id;
	private String nom;
	private String tag;
	private String lieu;
	private LocalDate dateDebut;
	private LocalDate dateFin;
	private String contenu;
	
	@DBRef
	private Professionnel professionnel;

}
