package com.aston.mobideal.dto;

import java.time.LocalDate;

import lombok.Data;


@Data
public class RechercheEvenement {
	
	private String tag;
	private String lieu;	
	private LocalDate date;

}
