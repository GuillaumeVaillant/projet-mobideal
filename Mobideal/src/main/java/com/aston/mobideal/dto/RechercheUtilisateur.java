package com.aston.mobideal.dto;

import lombok.Data;

@Data
public class RechercheUtilisateur {
	
	private String mail;
	private String password;
	
}
