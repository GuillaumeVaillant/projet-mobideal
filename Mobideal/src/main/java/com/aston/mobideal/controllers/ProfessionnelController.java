package com.aston.mobideal.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aston.mobideal.dto.RechercheProfessionel;
import com.aston.mobideal.models.Professionnel;
import com.aston.mobideal.services.ProfessionnelService;

@CrossOrigin
@RestController
@RequestMapping("professionnels")
public class ProfessionnelController {
	
	@Autowired
	private ProfessionnelService service;
	
	@GetMapping("")
	public List<Professionnel> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("")
	public Professionnel save(@RequestBody Professionnel entity) {
		return this.service.save(entity);
	}
	
	@PostMapping("saves")
	public List<Professionnel> saveAll(@RequestBody List<Professionnel> entities){
		return this.service.saveAll(entities);
	}
	
	@GetMapping("/id/{id}")
	public Optional<Professionnel> findById(@PathVariable String id){
		return this.service.findById(id);
	}
	
	@DeleteMapping("/id/{id}")
	public void deleteById(@PathVariable String id) {
		this.service.deleteById(id);
	}
	
	@GetMapping("/nom/{nom}")
	public Professionnel findByNom(@PathVariable String nom){		
		return this.service.findByNom(nom);
	}
	
	@PostMapping("/mail")
	public Optional<Professionnel> findByMailAndPassword(@RequestBody RechercheProfessionel rp){
		return this.service.findByMailAndPassword(rp);
	}

}
