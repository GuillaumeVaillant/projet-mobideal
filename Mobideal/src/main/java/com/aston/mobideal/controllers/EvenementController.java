package com.aston.mobideal.controllers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aston.mobideal.dto.RechercheEvenement;
import com.aston.mobideal.models.Evenement;
import com.aston.mobideal.services.EvenementService;

@RestController
@RequestMapping("evenements")
@CrossOrigin
public class EvenementController {
	
	@Autowired
	private EvenementService service;
	
	@GetMapping("")
	public List<Evenement> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("")
	public Evenement save(@RequestBody Evenement entity) {
		return this.service.save(entity);
	}
	
	@PostMapping("saves")
	public List<Evenement> saveAll(@RequestBody List<Evenement> entities){
		return this.service.saveAll(entities);
	}
	
	@GetMapping("/id/{id}")
	public Optional<Evenement> findById(@PathVariable String id){
		return this.service.findById(id);
	}
	
	@DeleteMapping("/id/{id}")
	public void deleteById(@PathVariable String id) {
		this.service.deleteById(id);
	}
		
	@PostMapping("recherches")
	public Stream<Evenement> findAnyOfTheseValues(@RequestBody RechercheEvenement re){
		return this.service.findAnyOfTheseValues(re);
	}
	@GetMapping("/nomProfessionnel/{nomProfessionnel}")
	public List<Evenement> findAllByNomProfessionnel(@PathVariable String nomProfessionnel){
		return this.service.findAllByNomProfessionnel(nomProfessionnel);
	}
	@GetMapping("/idProfessionnel/{idProfessionnel}")
	public List<Evenement> findAllByIdProfessionnel(@PathVariable String idProfessionnel){
		return this.service.findAllByIdProfessionnel(idProfessionnel);
	}
	
	@GetMapping("/nom/{nom}")
	public Optional<Evenement> findByNom(@PathVariable String nom){
		return this.service.findByNom(nom);
	}
	@GetMapping("lieu/{lieu}")
	public List<Evenement> findAllByLieu(@PathVariable String lieu){
		return this.service.findAllByLieu(lieu);
	}
}
