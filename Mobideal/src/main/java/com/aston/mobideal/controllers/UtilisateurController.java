package com.aston.mobideal.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aston.mobideal.dto.RechercheUtilisateur;
import com.aston.mobideal.models.Utilisateur;
import com.aston.mobideal.services.UtilisateurService;

@RestController
@RequestMapping("utilisateurs")
@CrossOrigin
public class UtilisateurController {
	
	@Autowired
	private UtilisateurService service;
	
	@GetMapping("")
	public List<Utilisateur> findAll(){
		return this.service.findAll();
	}
	
	@PostMapping("")
	public Utilisateur save(@RequestBody Utilisateur entity) {
		return this.service.save(entity);
	}
	
	@PostMapping("/saves")
	public  List<Utilisateur> saveAll(@RequestBody List<Utilisateur> entities){
		return this.service.saveAll(entities);
	}
	
	@GetMapping("/id/{id}")
	public Optional<Utilisateur> findById(@PathVariable String id) {
		return this.service.findById(id);
	}
	
	@DeleteMapping("/id/{id}")
	public void deleteById(@PathVariable String id) {
		this.service.deleteById(id);
	}
	@PostMapping("mail")
	public Optional<Utilisateur> findByMailAndPassword(@RequestBody RechercheUtilisateur ru){
		return this.service.findByMailAndPassword(ru);
	}
	
}
