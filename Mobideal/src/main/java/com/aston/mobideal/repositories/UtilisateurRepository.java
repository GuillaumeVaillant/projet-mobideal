package com.aston.mobideal.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.aston.mobideal.models.Utilisateur;

public interface UtilisateurRepository extends MongoRepository<Utilisateur, String> {
	
	public Optional<Utilisateur> findByMailAndPassword(String mail,String password);
	

}
