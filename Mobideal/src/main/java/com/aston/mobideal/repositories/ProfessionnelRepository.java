package com.aston.mobideal.repositories;


import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.aston.mobideal.models.Professionnel;

public interface ProfessionnelRepository extends MongoRepository<Professionnel, String>{
	
	public Professionnel findByNom(String nom);
	public Optional<Professionnel> findByMailAndPassword(String mail,String password);
	
	
	

}
