package com.aston.mobideal.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.aston.mobideal.models.Evenement;
import com.aston.mobideal.models.Professionnel;

public interface EvenementRepository extends MongoRepository<Evenement, String>{
	
	public List<Evenement> findAllByProfessionnel(Professionnel professionnels);
	public List<Evenement> findAllByLieu(String lieu);
	public Optional<Evenement> findByNom(String nom);
	public List<Evenement> findAllByProfessionnel(Optional<Professionnel> professionnel);
	
	
	
}
