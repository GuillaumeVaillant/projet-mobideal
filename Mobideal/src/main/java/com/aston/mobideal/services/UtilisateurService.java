package com.aston.mobideal.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aston.mobideal.dto.RechercheUtilisateur;
import com.aston.mobideal.models.Utilisateur;
import com.aston.mobideal.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {
	
	@Autowired
	private UtilisateurRepository repository;
	
	public Utilisateur save(Utilisateur entity) {
		LocalDate date = LocalDate.now();
		entity.setDateInscription(date);
		return this.repository.save(entity);		
	}
	
	public  List<Utilisateur> saveAll(List<Utilisateur> entities){
		LocalDate date = LocalDate.now();
		for(int i=0;i<entities.size();i++) {
			entities.get(i).setDateInscription(date);
		}
		return this.repository.saveAll(entities);
	}
	
	public List<Utilisateur> findAll(){
		return this.repository.findAll();
	}
	
	public Optional<Utilisateur> findById(String id){
		return this.repository.findById(id);
	}
	
	public void deleteById(String id) {
		this.repository.deleteById(id);
	}
	
	public Optional<Utilisateur> findByMailAndPassword(RechercheUtilisateur ru){
		return this.repository.findByMailAndPassword(ru.getMail(), ru.getPassword());
	}
	
	

}
