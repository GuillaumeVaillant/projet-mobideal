package com.aston.mobideal.services;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aston.mobideal.dto.RechercheProfessionel;
import com.aston.mobideal.models.Professionnel;
import com.aston.mobideal.repositories.ProfessionnelRepository;

@Service
public class ProfessionnelService {
	
	@Autowired
	private ProfessionnelRepository repository;
	
	public Professionnel save(Professionnel entity) {
		LocalDate date = LocalDate.now();
		entity.setDateInscription(date);
		return this.repository.save(entity);
	}
	
	public List<Professionnel> saveAll(List<Professionnel> entities){
		LocalDate date = LocalDate.now();
		for(int i=0;i<entities.size();i++) {
			entities.get(i).setDateInscription(date);
		}
		return this.repository.saveAll(entities);
	}
	
	public List<Professionnel> findAll(){
		return this.repository.findAll();
	}
	
	public Optional<Professionnel> findById(String id){
		return this.repository.findById(id);
	}
	
	public void deleteById(String id) {
		this.repository.deleteById(id);
	}
	
	public Professionnel findByNom(String nom){		
		return this.repository.findByNom(nom);
	}
	public Optional<Professionnel> findByMailAndPassword(RechercheProfessionel rp){
		return this.repository.findByMailAndPassword(rp.getMail(), rp.getPassword());
	}
	public Professionnel insert(Professionnel entity) {
		return this.repository.insert(entity);
	}

}
