package com.aston.mobideal.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aston.mobideal.dto.RechercheEvenement;
import com.aston.mobideal.models.Evenement;
import com.aston.mobideal.models.Professionnel;
import com.aston.mobideal.repositories.EvenementRepository;

@Service
public class EvenementService {

	@Autowired
	private EvenementRepository repository;
	
	@Autowired
	private ProfessionnelService ps;

	public Evenement save(Evenement entity) {
		return this.repository.save(entity);
	}

	public List<Evenement> saveAll(List<Evenement> entities) {
		return this.repository.saveAll(entities);
	}

	public List<Evenement> findAll() {
		return this.repository.findAll();
	}

	public Optional<Evenement> findById(String id) {
		return this.repository.findById(id);
	}
	
	public Optional<Evenement> findByNom(String nom) {
		return this.repository.findByNom(nom);
	}

	public void deleteById(String id) {
		this.repository.deleteById(id);
	}
	
	public List<Evenement> findAllByNomProfessionnel(String nomProfessionnel){
		Professionnel professionnel = this.ps.findByNom(nomProfessionnel);
		return this.repository.findAllByProfessionnel(professionnel);
	}
	public List<Evenement> findAllByIdProfessionnel(String idProfessionnel){
		Optional<Professionnel> professionnel = this.ps.findById(idProfessionnel);
		return this.repository.findAllByProfessionnel(professionnel);
	}
	
	public List<Evenement> findAllByLieu(String lieu){
		return this.repository.findAllByLieu(lieu);
	}

	public Stream<Evenement> findAnyOfTheseValues(RechercheEvenement re) {		
		return this.repository.findAll().stream().filter((evenement)->{
			boolean bTags = false,bLieux = false,bDates = false;
			if(re.getTag()!=null && !re.getTag().isEmpty()) {				
				bTags=evenement.getTag().equals(re.getTag());				
			}else {
				bTags=true;
			}
			if(re.getLieu()!=null && !re.getLieu().isEmpty()) {				
				bLieux=evenement.getLieu().equals(re.getLieu());							
			}else {
				bLieux=true;
			}		    
		    if(re.getDate()!=null) {		    			
		    	bDates=evenement.getDateDebut().isBefore(re.getDate())
		    				&&evenement.getDateFin().isAfter(re.getDate());		    		
		    }else {
		    	bDates=true;
		    }
		    return bTags&&bLieux&&bDates;
			
		});
		
		
	}

}
