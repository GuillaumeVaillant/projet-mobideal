package com.aston.mobideal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobidealApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobidealApplication.class, args);
	}

}
